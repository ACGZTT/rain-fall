﻿using System.Collections;
using Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Launch : MonoBehaviour
{
    private const string SceneName = "BattleScene";

    private void Start()
    {
        Instantiate(new GameObject("InputManager")).AddComponent<InputManager>();
        Instantiate(new GameObject("TCPClient")).AddComponent<TCPClient>();
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForEndOfFrame();
        SceneManager.LoadScene(SceneName);
        yield return null;
    }
}