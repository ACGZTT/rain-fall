﻿namespace Managers
{
    public interface IInput
    {
        public void OnInput(string input)
        {
            // Implement input handling logic here
        }
    }
}