﻿using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Utils;

namespace Managers
{
    public class InputData
    {
    }

    public class InputManager : MonoSingleton<InputManager>
    {
        List<IInput> _actions = new List<IInput>();

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        void Update()
        {
            if (Input.anyKeyDown)
            {
                foreach (IInput action in _actions)
                {
                    action.OnInput(Input.inputString);
                }
            }
        }

        public void Register(IInput obj)
        {
            if (!_actions.Contains(obj))
            {
                _actions.Add(obj);
            }
        }

        public void Unregister(IInput obj)
        {
            _actions.Remove(obj);
        }
    }
}