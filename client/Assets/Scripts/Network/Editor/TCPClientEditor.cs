using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TCPClient))]
public class TCPClientEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TCPClient tcpClient = (TCPClient)target;

        if (GUILayout.Button("Connect to Server"))
        {
            tcpClient.ConnectToServer();
        }
    }
}
