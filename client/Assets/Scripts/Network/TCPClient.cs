using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using Utils;

public class TCPClient : MonoSingleton<TCPClient>
{
    private Socket clientSocket;
    [SerializeField] private string serverIp = "127.0.0.1";
    [SerializeField] private int serverPort = 1234;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void ConnectToServer()
    {
        try
        {
            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(serverIp), serverPort);

            clientSocket.Connect(ipEndPoint);
            Debug.Log("[Client]: Connected to server!");

            SendMessageToServer("Hello from Unity client!");

            ReceiveMessageFromServer();
        }
        catch (Exception ex)
        {
            Debug.LogError("[Client]: Error connecting to server - " + ex.Message);
        }
    }

    private void SendMessageToServer(string message)
    {
        if (clientSocket != null && clientSocket.Connected)
        {
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            clientSocket.Send(messageBytes);
            Debug.Log("[Client]: Sent message: " + message);
        }
    }

    private void ReceiveMessageFromServer()
    {
        if (clientSocket != null && clientSocket.Connected)
        {
            byte[] buffer = new byte[1024];
            int bytesRead;

            clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback),
                buffer);
        }
    }

    private void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            byte[] buffer = (byte[])ar.AsyncState;
            int bytesRead = clientSocket.EndReceive(ar);

            if (bytesRead > 0)
            {
                string message = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Debug.Log("[Client]: Received message from server: " + message);

                ReceiveMessageFromServer();
            }
        }
        catch (Exception ex)
        {
            Debug.LogError("[Client]: Error receiving message - " + ex.Message);
        }
    }

    private void OnApplicationQuit()
    {
        if (clientSocket != null && clientSocket.Connected)
        {
            clientSocket.Close();
            Debug.Log("[Client]: Connection closed.");
        }
    }
}