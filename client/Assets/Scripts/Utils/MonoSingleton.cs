﻿using UnityEngine;

namespace Utils
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        // 这个变量保存单例实例
        private static T _instance;

        // 公共属性可以外部访问单例实例
        public static T Instance
        {
            get
            {
                // 如果实例为空，尝试查找场景中的已有实例
                if (_instance == null)
                {
                    // 查找场景中是否已有此类型的单例
                    _instance = FindObjectOfType<T>();

                    // 如果场景中没有实例，创建一个新的GameObject并附加上MonoBehaviour实例
                    if (_instance == null)
                    {
                        GameObject singletonObject = new GameObject(typeof(T).Name);
                        _instance = singletonObject.AddComponent<T>();
                        DontDestroyOnLoad(singletonObject); // 保证切换场景时，单例不会被销毁
                    }
                }

                return _instance;
            }
        }

        // 防止外部创建多个实例
        protected virtual void Awake()
        {
            // 如果已经有实例存在，销毁当前实例
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                _instance = this as T;
            }
        }

        // 如果需要，可以在这里释放资源
        protected virtual void OnDestroy()
        {
            if (_instance == this)
            {
                _instance = null;
            }
        }
    }
}