using UnityEngine;
using System.Collections.Generic;

namespace Utils
{
    // 限制 T 为 MonoBehaviour 类型
    public class RainfallGameObjectPool<T> where T : MonoBehaviour
    {
        // 用于存储空闲的对象
        private readonly Stack<T> _pool;
        // 对象池的最大容量
        private readonly int _maxSize;

        // 构造函数
        public RainfallGameObjectPool(int maxSize = 10)
        {
            _pool = new Stack<T>(maxSize);
            _maxSize = maxSize;
        }

        // 获取一个对象，如果池为空，则创建一个新的对象
        public T Get()
        {
            T obj;
            if (_pool.Count > 0)
            {
                // 从池中获取一个对象
                obj = _pool.Pop();
                obj.gameObject.SetActive(true);
            }
            else
            {
                // 如果池为空，则创建一个新的对象
                GameObject newObj = new GameObject();
                obj = newObj.AddComponent<T>();
            }
            return obj;
        }

        // 将对象返回池中
        public void Release(T obj)
        {
            if (_pool.Count < _maxSize)
            {
                // 将对象放回池中
                obj.gameObject.SetActive(false);
                _pool.Push(obj);
            }
            else
            {
                // 如果池已满，销毁对象
                GameObject.Destroy(obj.gameObject);
            }
        }

        // 清空对象池
        public void Clear()
        {
            while (_pool.Count > 0)
            {
                GameObject.Destroy(_pool.Pop().gameObject);
            }
        }

        // 获取池中的当前对象数量
        public int Count => _pool.Count;

        // 获取池的最大容量
        public int MaxSize => _maxSize;
    }
}