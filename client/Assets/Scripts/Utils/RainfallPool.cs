﻿using System.Collections.Generic;

namespace Utils
{
    public class RainfallPool<T> where T : new()
    {
        // 用于存储空闲的对象
        private readonly Stack<T> _pool;

        // 对象池的最大容量
        private readonly int _maxSize;

        // 构造函数
        public RainfallPool(int maxSize = 10)
        {
            _pool = new Stack<T>(maxSize);
            _maxSize = maxSize;
        }

        // 获取一个对象，如果池为空，则创建一个新的对象
        public T Get()
        {
            if (_pool.Count > 0)
            {
                // 从池中获取一个对象
                return _pool.Pop();
            }

            // 如果池为空，则创建一个新的对象
            return new T();
        }

        // 将对象返回池中
        public void Release(T obj)
        {
            if (_pool.Count < _maxSize)
            {
                // 将对象放回池中
                _pool.Push(obj);
            }
            else
            {
                // 如果池已满，销毁对象
                // 在这里可以实现对象销毁的逻辑
                // 比如：释放资源、清空缓存等
            }
        }

        // 清空对象池
        public void Clear()
        {
            _pool.Clear();
        }

        // 获取池中的当前对象数量
        public int Count => _pool.Count;

        // 获取池的最大容量
        public int MaxSize => _maxSize;
    }
}