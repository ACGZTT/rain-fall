﻿using System;
using System.IO;
using Entities;
using UnityEditor;
using UnityEngine;

namespace Scenes
{
    public class BattleScene : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private TerrainGenerator _terrainGenerator;

        void Start()
        {
            _terrainGenerator.Init();
            _terrainGenerator.SetPlayer(_player);
        }
    }
}