using UnityEngine;

namespace Entities
{
    public class RFTerrainData
    {
        public RFTerrainData()
        {
        }

        private TerrainData terrainData;

        public TerrainData TerrainData
        {
            get => terrainData;
            set => terrainData = value;
        }

        private Vector3 position;

        public Vector3 Position
        {
            get => position;
            set => position = value;
        }
    }
}