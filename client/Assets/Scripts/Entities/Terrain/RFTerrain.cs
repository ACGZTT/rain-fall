using UnityEngine;

namespace Entities
{
    public class RFTerrain
    {
        private GameObject _chunk;
        private Terrain _terrain;
        private TerrainCollider _terrainCollider;
        private RFTerrainData _rfTerrainData;

        public RFTerrain(RFTerrainData rfTerrainData)
        {
            _chunk = new GameObject("terrain chunk");
            _terrain = _chunk.AddComponent<Terrain>();
            _terrain.materialTemplate = new Material(Shader.Find("Nature/Terrain/Standard"));
            _terrainCollider = _chunk.AddComponent<TerrainCollider>();
            _rfTerrainData = rfTerrainData;
            _chunk.transform.position = _rfTerrainData.Position;
            _terrain.terrainData = _rfTerrainData.TerrainData;
            _terrainCollider.terrainData = _rfTerrainData.TerrainData;
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(_chunk);
        }
    }
}