using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class TerrainGenerator : MonoBehaviour
    {
        public int chunkSize = 50; // terrain chunk size
        public int renderDistance = 3; // reder distance
        public float heightMultiplier = 5f; // height multiplier

        private Player player; // player object
        private Vector3 _lastPlayerPosition; // last player pos
        private Vector3 _playerPosition; // cur player pos
        private readonly float _terrainWidth = 256f; // single terrain width
        private readonly float _terrainHeight = 256f; // single terrain width

        private Dictionary<Vector2, RFTerrain> _terrainChunks = new Dictionary<Vector2, RFTerrain>(); // loaded chunks

        public void Init()
        {
            UpdateTerrainChunks();
        }

        public void SetPlayer(Player p)
        {
            player = p;
            _lastPlayerPosition = player.transform.position;
        }

        void Update()
        {
            if (player != null) _playerPosition = player.transform.position;

            if (Vector3.Distance(_playerPosition, _lastPlayerPosition) >= chunkSize)
            {
                _lastPlayerPosition = _playerPosition;
                UpdateTerrainChunks();
            }
        }

        void UpdateTerrainChunks()
        {
            Vector2 playerChunkPosition;
            if (player != null)
            {
                playerChunkPosition = new Vector2(Mathf.Floor(_playerPosition.x / chunkSize),
                    Mathf.Floor(_playerPosition.z / chunkSize));
            }
            else
            {
                playerChunkPosition = Vector2.zero;
            }

            List<Vector2> chunksToUnload = new List<Vector2>();
            for (int xOffset = -renderDistance; xOffset <= renderDistance; xOffset++)
            {
                for (int zOffset = -renderDistance; zOffset <= renderDistance; zOffset++)
                {
                    Vector2 chunkPosition = playerChunkPosition + new Vector2(xOffset, zOffset);

                    if (!_terrainChunks.ContainsKey(chunkPosition))
                    {
                        RFTerrain chunk = GenerateTerrainChunk(chunkPosition);
                        _terrainChunks.Add(chunkPosition, chunk);
                    }

                    foreach (var chunk in _terrainChunks)
                    {
                        if (Vector2.Distance(chunk.Key, playerChunkPosition) > renderDistance)
                        {
                            chunksToUnload.Add(chunk.Key);
                        }
                    }
                }
            }

            foreach (var chunkPosition in chunksToUnload)
            {
                UnloadTerrainChunk(chunkPosition);
            }
        }

        RFTerrain GenerateTerrainChunk(Vector2 chunkPosition)
        {
            RFTerrainData rfTerrainData = new RFTerrainData();
            rfTerrainData.Position = new Vector3(chunkPosition.x * chunkSize, 0, chunkPosition.y * chunkSize);

            TerrainData terrainData = new TerrainData();
            terrainData.heightmapResolution = chunkSize + 1;
            terrainData.SetHeights(0, 0, GenerateHeights(chunkPosition));
            rfTerrainData.TerrainData = terrainData;

            RFTerrain chunk = new RFTerrain(rfTerrainData);
            return chunk;
        }

        void UnloadTerrainChunk(Vector2 chunkPosition)
        {
            if (_terrainChunks.ContainsKey(chunkPosition))
            {
                _terrainChunks[chunkPosition].Dispose();
                _terrainChunks.Remove(chunkPosition);
            }
        }

        float[,] GenerateHeights(Vector2 chunkPosition)
        {
            float[,] heights = new float[chunkSize, chunkSize];
            for (int x = 0; x < chunkSize; x++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    float xCoord = (chunkPosition.x * chunkSize + x) / _terrainWidth;
                    float zCoord = (chunkPosition.y * chunkSize + z) / _terrainHeight;
                    heights[x, z] = Mathf.PerlinNoise(xCoord, zCoord) * heightMultiplier;
                }
            }

            return heights;
        }
    }
}