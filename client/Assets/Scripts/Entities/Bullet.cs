using System;
using UnityEngine;

namespace Entities
{
    public class Bullet : MonoBehaviour
    {
        private const string BulletPrefabPath = "Prefabs/bullet";
        private GameObject _bulletGo;
        private Vector3 _moveDirection;
        private bool _move = false;
        private float _speed = 1.0f;

        private void Start()
        {
            _bulletGo = Resources.Load<GameObject>(BulletPrefabPath);
        }

        public void Init(Transform weapon)
        {
            Instantiate(_bulletGo, weapon.transform.position, weapon.transform.rotation);
            _moveDirection = Player.Instance.FaceOrient;
            _move = true;
        }

        private void Update()
        {
            if (_move)
            {
                Move();
            }
        }

        void Move()
        {
            Vector3 position = _bulletGo.transform.position;
            Vector3 movement = _moveDirection * _speed * Time.deltaTime;
            _bulletGo.transform.position = position + movement;
        }
    }
}