using UnityEngine;
using UnityEngine.Pool;
using Utils;

namespace Entities
{
    public class Gun : MonoBehaviour
    {
        public Bullet bullet; // 子弹
        public Transform firePoint; // 发射点
        public float fireRate = 1.0f; // 射速，每秒发射的子弹数
        private float _nextFireTime = 0f; // 下一次可以发射的时间
        private bool _isFiring = false; // 是否正在持续射击
        private RainfallGameObjectPool<Bullet> _bulletPool = new RainfallGameObjectPool<Bullet>();

        void Update()
        {
            if (Input.GetButtonDown("Fire1")) // 按下鼠标左键
            {
                Fire();
                _isFiring = true;
            }

            if (Input.GetButtonUp("Fire1")) // 松开鼠标左键
            {
                _isFiring = false;
            }

            if (_isFiring && Time.time >= _nextFireTime) // 持续射击且满足射速条件
            {
                Fire();
            }
        }

        void Fire()
        {
            if (Time.time >= _nextFireTime)
            {
                var bullet = _bulletPool.Get();
                bullet.Init(this.transform);
                _nextFireTime = Time.time + 1f / fireRate;
            }
        }
    }
}