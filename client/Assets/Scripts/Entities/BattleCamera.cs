using System;
using UnityEngine;

namespace Entities
{
    public class BattleCamera : MonoBehaviour
    {
        public float heightOffset = 2.0f;
        private Player _player;

        private void Update()
        {
            if (_player == null)
            {
                _player = Player.Instance;
            }

            if (_player != null)
            {
                transform.position = new Vector3(_player.transform.position.x,
                    _player.transform.position.y + heightOffset,
                    _player.transform.position.z);
                transform.LookAt(_player.transform);
            }
        }
    }
}