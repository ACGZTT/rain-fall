﻿using System;
using Managers;
using UnityEngine;

namespace Entities
{
    [RequireComponent(typeof(Rigidbody))]
    public class Player : MonoBehaviour
    {
        public float moveSpeed = 10f;
        public float rotationSpeed = 10f;
        public float jumpForce = 5f;
        private Rigidbody rb;
        private bool isGrounded;
        public LayerMask groundLayer;
        private static Player _instance;
        public static Player Instance => _instance;
        public Vector3 FaceOrient;

        void Start()
        {
            rb = GetComponent<Rigidbody>();
            _instance = this;
            FaceOrient = new Vector3(1, 0, 0);
        }

        void Update()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector3 moveDirection = new Vector3(horizontal, 0f, vertical).normalized;

            if (moveDirection.magnitude >= 0.1f)
            {
                MovePlayer(moveDirection);
            }

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                Jump();
            }

            CheckGroundStatus();
        }

        private void MovePlayer(Vector3 direction)
        {
            Vector3 move = direction * moveSpeed * Time.deltaTime;
            rb.MovePosition(transform.position + move);
            if (direction != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(direction);
                transform.rotation =
                    Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            }
        }

        private void Jump()
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        private void CheckGroundStatus()
        {
            // 使用射线检测玩家脚下是否接触地面
            isGrounded = Physics.Raycast(transform.position, Vector3.down, 0.1f, groundLayer);
        }
    }
}