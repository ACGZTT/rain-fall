﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace server
{
    class ServerEntry
    {
        public static void Main(string[] args)
        {
            Socket listenfd = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ipAdr = IPAddress.Parse("127.0.0.1");
            IPEndPoint ipEP = new IPEndPoint(ipAdr, 1234);
            listenfd.Bind(ipEP);
            listenfd.Listen(0);
            Console.WriteLine("[Server]: Start success.");

            while (true)
            {
                Socket connfd = listenfd.Accept();
                Console.WriteLine("[Server]: Client connected.");

                System.Threading.Thread clientThread = new System.Threading.Thread(() => HandleClient(connfd));
                clientThread.Start();
            }
        }

        private static void HandleClient(Socket connfd)
        {
            try
            {
                byte[] buffer = new byte[1024];
                int bytesRead;

                while ((bytesRead = connfd.Receive(buffer)) > 0)
                {
                    string receivedMessage = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    Console.WriteLine("[Server]: Received message from client: " + receivedMessage);

                    string responseMessage = "Server received: " + receivedMessage;
                    byte[] responseBytes = Encoding.ASCII.GetBytes(responseMessage);
                    connfd.Send(responseBytes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Server]: Error in communication: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("[Server]: Closing connection.");
                connfd.Close();
            }
        }
    }
}